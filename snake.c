#include <ncurses.h> // ncurses includes stdio
#include <pthread.h>
#include <stdlib.h>
#include <time.h>

// #define SNK_SPD 17000000 // nanoseconds for sleeping
#define SNAKE_PAIR 4
#define FOOD_PAIR 3
#define ROWS 41
#define COLS 190
#define SNK_L 10

char DISPLAY[ROWS][COLS];

// let's say that snake can be at maximum length of 100
typedef struct snake_part {
    int x;
    int y;
    enum direction { up, down, left, right } direc;
} snake_part;

struct snake {
    unsigned char snake_len;
    snake_part body[100];
} snake; // defining the snake global variable

struct food {
    int x;
    int y;
} snfd; // defining the food global var

void init_board() {
    // init the DISPLAY i made ^_^
    for (int i = 0; i < ROWS; ++i)
        for (int j = 0; j < COLS; ++j)
            DISPLAY[i][j] = ' ';
    snake.snake_len = SNK_L;
    // Starting point in the middle of screen
    snake.body[0].x = COLS / 2 - 1;
    snake.body[0].y = ROWS / 2;
    snake.body[0].direc = right;
    for (int i = 1; i < SNK_L; ++i) {
        snake.body[i].y = ROWS / 2;
        snake.body[i].x = COLS / 2 - i;
        snake.body[i].direc = right;
        DISPLAY[ROWS / 2][COLS / 2 - i] = 'S';
    }
}

pthread_mutex_t pause_mutex = PTHREAD_MUTEX_INITIALIZER; // this macro initializes the mutex
pthread_mutex_t drop_fd_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t food_eaten = PTHREAD_MUTEX_INITIALIZER;

void winner() {
    clear();
    mvwprintw(stdscr, ROWS / 2 - 1, COLS / 2 - 1, "We Have A Winner\n\t\t\t\t\t\t\t\t");
    printf("  \033[0;36mShove this trophy up your ass \033[0;31m.!.");
    refresh();
    getchar();
    curs_set(1); // set the cursor visible because my terminal is affected
    endwin();
    exit(0);
}
/*
 * @brief doing all the necessary stuff like firstly change direction of each
 * piece, then adjust(+,-), then draw
 */
void move_snake() {
    if (snake.body[0].x == snfd.x && snake.body[0].y == snfd.y) {
        pthread_mutex_lock(&drop_fd_mutex);
        if (++snake.snake_len == 100)
            winner();
        pthread_mutex_unlock(&food_eaten);
        pthread_mutex_unlock(&drop_fd_mutex);
    } else {
        mvaddch(snfd.y, snfd.x,
                'F'); // writes in stdscr which is actually myWin and i didn't know it
        DISPLAY[snake.body[snake.snake_len - 1].y][snake.body[snake.snake_len - 1].x] = ' ';
    }
    mvaddch(snake.body[snake.snake_len - 1].y, snake.body[snake.snake_len - 1].x, ' ');
    for (int i = snake.snake_len - 1; i > 0;
         --i) { // oura pros kefali klhron twn x,y kai klhron direction
        snake.body[i].x = snake.body[i - 1].x;
        snake.body[i].y = snake.body[i - 1].y;
        mvaddch(snake.body[i].y, snake.body[i].x, 'S');
        snake.body[i].direc = snake.body[i - 1].direc;
    }
    DISPLAY[snake.body[0].y][snake.body[0].x] = 'S';
    switch (snake.body[0].direc) {
    case up:
        (snake.body[0].y == 0) ? snake.body[0].y = (ROWS - 1) : --snake.body[0].y;
        break;
    case down:
        (snake.body[0].y == (ROWS - 1)) ? snake.body[0].y = 0 : ++snake.body[0].y;
        break;
    case left:
        (snake.body[0].x == 0) ? snake.body[0].x = (COLS - 1) : --snake.body[0].x;
        break;
    case right:
        (snake.body[0].x == (COLS - 1)) ? snake.body[0].x = 0 : ++snake.body[0].x;
        break;
    default:
        break;
    }
    mvaddch(snake.body[0].y, snake.body[0].x, 'S');
    if (DISPLAY[snake.body[0].y][snake.body[0].x] == 'S') {
        clear();
        mvprintw(20, 64,
                 "Kwsta nooba exases!!!!\n\n\t\t\t\t\t\t\t\t\t"
                 "Continue - \'C\'\n\n\t\t\t\t\t\t\t\t\t"
                 "Restart - \'R\'\n\n\t\t\t\t\t\t\t\t\t"
                 "Quit - \'Q\'\n\n\t\t\t\t\t\t\t\t\t\n");
        refresh();
        int c;
    getagain:
        c = getchar();
        if (c == 'C') {
            clear();
            return;
        } else if (c == 'R') {
            clear();
            init_board();
        } else if (c == 'Q') {
            clear();
            mvprintw(20, 70, " ");
            printf("\033[0;31mGoodbye Loser\033[0;m :/");
            refresh();
            getchar();
            curs_set(1); // set the cursor visible because my terminal is affected
            endwin();
            exit(0);
        } else {
            goto getagain;
        }
    }
}

void *drop_food(void *lala) {
    srand(time(NULL));
    while (1) {
        pthread_mutex_lock(&pause_mutex);
        pthread_mutex_lock(&drop_fd_mutex);
        snfd.x = rand() % COLS;
        snfd.y = rand() % ROWS;
        pthread_mutex_unlock(&drop_fd_mutex);
        pthread_mutex_unlock(&pause_mutex);
        if (pthread_mutex_trylock(&food_eaten)) { // wait on snake head to touch the food
            time_t start = time(0);
            while (
                food_eaten.__data.__lock &&
                (difftime(time(0), start) < 5)) { // if some time passed clear prev and drop new food
                for (int i = 0; i < 60000; ++i)
                    ; // spin while waiting
            }
            mvaddch(snfd.y, snfd.x, ' ');
            pthread_mutex_unlock(&food_eaten);
        }
    }
    return NULL;
}

int main() {
    init_board();
    initscr();
    clear();
    noecho();
    curs_set(0);          // set the cursor invisible
    keypad(stdscr, true); // enable arrow key auto escape so they are useable
    int c;
    // struct timespec wt; // sleeping is expensive
    // wt.tv_nsec = SNK_SPD;
    pthread_t food_thr;
    pthread_create(&food_thr, NULL, drop_food, stdscr);
playing_loop:
    nodelay(stdscr, true);
    while (1) {
        // cbreak(); /* Line buffering disabled. pass on everything */
        c = getch();
        if (c == 'P') {
            break;
        }
        switch (c) {
        case 'w':
        case 'W':
        case KEY_UP:
            if (snake.body[0].direc != down)
                snake.body[0].direc = up;
            break;
        case 's':
        case 'S':
        case KEY_DOWN:
            if (snake.body[0].direc != up)
                snake.body[0].direc = down;
            break;
        case 'a':
        case 'A':
        case KEY_LEFT:
            if (snake.body[0].direc != right)
                snake.body[0].direc = left;
            break;
        case 'd':
        case 'D':
        case KEY_RIGHT:
            if (snake.body[0].direc != left)
                snake.body[0].direc = right;
            break;
        default:
            break;
        }
        // clear(); i don't need to clear because it blinks and is slow
        move_snake(); // Also draws it
        refresh();
        // nanosleep(&wt, NULL);
        // spin 15 mil ~0.015 sec
        for (int i = 0; i < 5000000; i++) {
            ;
        }
    }
pause:
    pthread_mutex_lock(&pause_mutex);
    clear();
    mvwprintw(stdscr, 21, 70, "Game Paused!\n\t\t\t\t\t\t\t\t");
    printf(" \033[0;33mContinue? : \033[0;32mY[ES] \033[0;m/ "
           "\033[0;31mN[O]\033[0;m");
    refresh();
    c = getchar();
    if (c == 'Y') {
        pthread_mutex_unlock(&pause_mutex);
        clear();
        goto playing_loop;
    } else if (c != 'N') {
        pthread_mutex_unlock(&pause_mutex);
        goto pause;
    }
    curs_set(1); // set the cursor visible because my terminal is affected
    endwin();
    return 0;
}
