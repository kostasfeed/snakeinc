# .SILENT: snake

snake: snake.c
	gcc snake.c -l ncurses -pthread -o snake

clean:
	rm snake -f
